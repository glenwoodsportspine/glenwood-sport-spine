Whether you’re an active individual or a weekend warrior, our personalized treatments will get you moving and feeling better. We deal with all major insurance companies, many of our services can be directly billed with little to no out-of-pocket expense. We are also Motor Vehicle Accident and WCB-ap.

Address: 16416 100 Ave NW, Edmonton, AB T5P 4Y2, Canada

Phone: 780-487-6161

Website: https://gsshealth.ca
